#!/bin/bash
invalMsg="No testing required"
text="${1:0:8}"
if [ "$text" == "solution" ]; then
    echo "pytest test_level${1:14:18}"
else
    echo "echo $invalMsg"
fi
